***# idp-on-ocp3.11***

*# Prerequisites:*
- OCP 3.11 installed

*# Steps*

  1 - On your master host(s), ensure that you are logged in as a cluster administrator or a user with project administrator access to the global openshift project.

      $ oc login -u system:admin

  2 - Run the following commands to update the core set of Red Hat Single Sign-On 7.3.0.GA resources for OpenShift in the openshift project.

      $ for resource in sso73-image-stream.json \
        sso73-https.json \
        sso73-postgresql.json \
        sso73-postgresql-persistent.json \
        sso73-x509-https.json \
        sso73-x509-postgresql-persistent.json
      do
        oc replace -n openshift --force -f \
        https://raw.githubusercontent.com/jboss-container-images/redhat-sso-7-openshift-image/sso73-dev/templates/${resource}
      done

  3 - Make sure that the secret to authenticate at *registry.redhat.io* is in Openshift project *openshift*.

      $ oc create secret generic my-rh-credentials --from-file=.dockerconfigjson=$HOME/.docker/config.json --type=kubernetes.io/dockerconfigjson -n openshift

  4 - Create a new project

      $ oc new-project sso-demo

  5 - Run the following command to install the Red Hat Single Sign-On 7.3.0.GA OpenShift image streams in the openshift project.

      $ oc import-image redhat-sso73-openshift:1.0 -n sso-demo

  6 - Add the view role to the default service account. This enables the service account to view all the resources in the sso-app-demo namespace, which is necessary for managing the cluster.

     $ oc policy add-role-to-user view system:serviceaccount:$(oc project -q):default

  7 - List the available Red Hat Single Sign-On application templates.

     $ oc get templates -n openshift -o name | grep -o 'sso73.\+'
     sso73-https
     sso73-postgresql
     sso73-postgresql-persistent
     sso73-x509-https
     sso73-x509-postgresql-persistent

  7 - Deploy the selected one

     $ oc new-app --template=sso73-x509-postgresql-persistent -e SSO_ADMIN_USERNAME="admin" -e SSO_ADMIN_PASSWORD="password"
     --> Deploying template "openshift/sso73-x509-postgresql-persistent" to project sso-demo

         Red Hat Single Sign-On 7.3 on OpenJDK + PostgreSQL (Persistent)
         ---------
         An example application based on RH-SSO 7.4 on OpenJDK image. For more information about using this template, see https://github.com/jboss-container-images/redhat-sso-7-openshift-image/tree/sso73-dev/docs.

         A new persistent RH-SSO service (using PostgreSQL) has been created in your project. The admin username/password for accessing the master realm via the RH-SSO console is XpwRr5Ow/nV6HQtyqygcn08KEVsxh8AO3Gjkme8FX. The username/password for accessing the PostgreSQL database "root" is userEgX/fxMdihiNUYtHbYpYa1HIFwS1SJyfoQ40. The HTTPS keystore used for serving secure content, the JGroups keystore used for securing JGroups communications, and server truststore used for securing RH-SSO requests were automatically created via OpenShift's service serving x509 certificate secrets.

          * With parameters:
             * Application Name=sso
             * Custom RH-SSO Server Hostname=
             * JGroups Cluster Password=tHWwCSGEtxdoVxQr0SCbpelOncLUMukh # generated
             * Database JNDI Name=java:jboss/datasources/KeycloakDS
             * Database Name=root
             * Datasource Minimum Pool Size=
             * Datasource Maximum Pool Size=
             * Datasource Transaction Isolation=
             * PostgreSQL Maximum number of connections=
             * PostgreSQL Shared Buffers=
             * Database Username=userEgX # generated
             * Database Password=fxMdihiNUYtHbYpYa1HIFwS1SJyfoQ40 # generated
             * Database Volume Capacity=1Gi
             * ImageStream Namespace=openshift
             * RH-SSO Administrator Username=XpwRr5Ow # generated
             * RH-SSO Administrator Password=nV6HQtyqygcn08KEVsxh8AO3Gjkme8FX # generated
             * RH-SSO Realm=
             * RH-SSO Service Username=
             * RH-SSO Service Password=
             * PostgreSQL Image Stream Tag=10
             * Container Memory Limit=1Gi
     --> Creating resources ...
         service "sso" created
         service "sso-postgresql" created
         service "sso-ping" created
         route.route.openshift.io "sso" created
         deploymentconfig.apps.openshift.io "sso" created
         deploymentconfig.apps.openshift.io "sso-postgresql" created
         persistentvolumeclaim "sso-postgresql-claim" created
     --> Success
         Access your application via route 'sso-sso-app-demo.poc.rhlab.de'
         Run 'oc status' to view your app.



Link status detection 100ms (for fast failover)
