#!/bin/bash -e
source bin/env.sh

KEYCLOAK_URL="https://sso-sso-frankfurt.poc.rhlab.de/auth"
SERVICE_URL="https://demo-service-keycloak.poc.rhlab.de"

echo $RHSSO_URL
echo $SERVICE_URL

oc get buildconfigs demo-app &>/dev/null || oc new-build --binary=true --name=demo-app
oc start-build demo-app --from-dir=demo-app --follow

oc new-app -f demo-app/demo-app.json -p KEYCLOAK_URL=$KEYCLOAK_URL -p SERVICE_URL=$SERVICE_URL
